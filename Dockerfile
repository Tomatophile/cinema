# BUILD STAGE
FROM maven:3.8.1-jdk-11 as build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN mvn clean package -DskipTests

# RUNTIME STAGE
FROM adoptopenjdk/openjdk11:alpine-jre
RUN mkdir /app
COPY --from=build /project/target/*.jar /app/java-application.jar
WORKDIR /app
CMD "java" "-jar" "java-application.jar"
